package co.simplon.cda.entity;

/**
 * La classe cat avec plusieurs méthodes sans rapport.
 */
public class Cat {
  private boolean isClean = false;
  private String name;
  private String breed;

  public String bath() {
    if (isClean == false) {
      isCleaning();
      return "Le chat est propre !";
    } else {
      return "Le chat est déjà propre !";
    }
  }

  private void isCleaning() {
    isClean = true;
    System.out.println("Le chat est en train de sa laver.");
  }

  public String presentation() {
   return "Salut ! Je m'appelle " + name + " et je suis un " + breed;
  }
}
