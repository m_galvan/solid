package co.simplon.cda.entity;

/**
 * La classe CatSmellGood permet au chat d'être propre.
 */
public class CatSmellGood {
  private boolean isClean = false;

  /**
   * Cette méthode permet au chat prendre son bain.
   */
  public String bath() {
    if (isClean == false) {
      isCleaning();
      return "Le chat est propre !";
    } else {
      return "Le chat est déjà propre !";
    }
  }

  /**
   * Cette méthode nettoie le chat. Elle est appelée dans le bain.
   */
  private void isCleaning() {
    isClean = true;
  }
}
