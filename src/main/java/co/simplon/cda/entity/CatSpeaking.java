package co.simplon.cda.entity;

/**
 * La classe CatSpeaking ne concerne que les dialogues du chat.
 */
public class CatSpeaking {
  private String name;
  private String breed;

  /**
   * Permet au chat de se présenter.
   */
  public String presentation() {
    return "Salut ! Je m'appelle " + name + " et je suis un " + breed;
  }
}
